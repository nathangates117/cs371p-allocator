// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.hpp"

using namespace testing;

// -----------------
// AllocatorFixture1
// -----------------

template <typename T>
struct AllocatorFixture1 : Test {
    // ------
    // usings
    // ------

    using allocator_type = T;
    using value_type     = typename T::value_type;
    using size_type      = typename T::size_type;
    using pointer        = typename T::pointer;
};

using allocator_types_1 =
    Types<
    std::allocator<int>,
    std::allocator<double>,
    my_allocator<int,    100>,
    my_allocator<double, 100>>;

#ifdef __APPLE__
TYPED_TEST_CASE(AllocatorFixture1, allocator_types_1,);
#else
TYPED_TEST_CASE(AllocatorFixture1, allocator_types_1);
#endif

TYPED_TEST(AllocatorFixture1, test0) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(*p, v);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(AllocatorFixture1, test1) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// -----------------
// AllocatorFixture2
// -----------------

template <typename T>
struct AllocatorFixture2 : Test {
    // ------
    // usings
    // ------

    using allocator_type = T;
    using value_type     = typename T::value_type;
    using size_type      = typename T::size_type;
    using pointer        = typename T::pointer;
};

using allocator_types_2 =
    Types<
    my_allocator<int,    100>,
    my_allocator<double, 100>>;

#ifdef __APPLE__
TYPED_TEST_CASE(AllocatorFixture2, allocator_types_2,);
#else
TYPED_TEST_CASE(AllocatorFixture2, allocator_types_2);
#endif

TYPED_TEST(AllocatorFixture2, test0) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(*p, v);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(AllocatorFixture2, test1) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

TYPED_TEST(AllocatorFixture2, test2) {
    using allocator_type = typename TestFixture::allocator_type;

    allocator_type x;
    ASSERT_EQ(x[0], 92);
}                                         // fix test -- yes

TYPED_TEST(AllocatorFixture2, test3) {
    using allocator_type = typename TestFixture::allocator_type;

    const allocator_type x;
    ASSERT_EQ(x[0], 92);
}                                         // fix test

// ------------ our unit tests -- HOW DO
// one for split
// one to make sure merging occurred
// one for iterator begin
// one for iterator end
template <typename T>
struct AllocatorFixture3 : Test {
    // ------
    // usings
    // ------

    using allocator_type = T;
    using value_type     = typename T::value_type;
    using size_type      = typename T::size_type;
    using pointer        = typename T::pointer;
};

using allocator_types_3 =
    Types<
    my_allocator<int,    100>,
    my_allocator<double, 100>>;

#ifdef __APPLE__
TYPED_TEST_CASE(AllocatorFixture3, allocator_types_3,);
#else
TYPED_TEST_CASE(AllocatorFixture3, allocator_types_3);
#endif

// Testing valid - after the allocations, we should have a valid heap state.
TYPED_TEST(AllocatorFixture3, validTest0) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
    ASSERT_EQ(x[0], 92);
    ASSERT_EQ(*--(x.end()), 92);
}

// // Testing iterator begin and end
TYPED_TEST(AllocatorFixture3, iterators0) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
    ASSERT_EQ(x[0], *x.begin());
    ASSERT_EQ(x[96], *--(x.end()));
}

// const_iterator begin
TYPED_TEST(AllocatorFixture3, splitTest0) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 5;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        if (sizeof(value_type) == sizeof(int)) {
            ASSERT_EQ(*(x.begin()), -20);
            ASSERT_EQ(*++(x.begin()), 64);
        } else if (sizeof(value_type) == sizeof(double)) {
            ASSERT_EQ(*(x.begin()), -40);
            ASSERT_EQ(*++(x.begin()), 44);
        }
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// // test that both merge left and right work by having free blocks on either side of a busy block and free the busy block
TYPED_TEST(AllocatorFixture3, mergeTest0) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 2;
    const value_type v = 2;
    const pointer b = x.allocate(s);
    const pointer b2 = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
            if (b2 != nullptr) {
                pointer e2 = b2 + s;
                pointer p2 = b2;
                try {
                    while (p2 != e2) {
                        x.construct(p2, v);
                        ++p2;
                    }
                }
                catch (...) {
                    while (b2 != p2) {
                        --p2;
                        x.destroy(p2);
                    }
                    x.deallocate(b2, s);
                    throw;
                }
                while (b2 != e2) {
                    --e2;
                    x.destroy(e2);
                }
                x.deallocate(b2, s);
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
        ASSERT_EQ(x[0], 92);
    }

}

// // test that the REPL helper method which finds the deallocation pointer is equal to the actual pointer we want to deallocate.
TYPED_TEST(AllocatorFixture3, REPLfindDeallocatePointer0) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 2;
    const value_type v = 2;
    const pointer b = x.allocate(s);
    const pointer b2 = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
            if (b2 != nullptr) {
                pointer e2 = b2 + s;
                pointer p2 = b2;
                try {
                    while (p2 != e2) {
                        x.construct(p2, v);
                        ++p2;
                    }
                }
                catch (...) {
                    while (b2 != p2) {
                        --p2;
                        x.destroy(p2);
                    }
                    x.deallocate(b2, s);
                    throw;
                }
                ASSERT_EQ(x.findDeallocateBlock(2), b2);
                while (b2 != e2) {
                    --e2;
                    x.destroy(e2);
                }
                x.deallocate(b2, s);
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
        ASSERT_EQ(x[0], 92);
    }
}

// TYPED_TEST(AllocatorFixture2, test2) {
//     using allocator_type = typename TestFixture::allocator_type;

//     allocator_type x;
//     ASSERT_EQ(x[0], 92);}                                         // fix test

// TYPED_TEST(AllocatorFixture2, test3) {
//     using allocator_type = typename TestFixture::allocator_type;

//     const allocator_type x;
//     ASSERT_EQ(x[0], 92);}                                         // fix test


