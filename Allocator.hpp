// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator { // DO WE HAVE TO GENERALIZE TO TYPE T OR JUST USE THE INT* -- if generalize, change all *4 arithmetic to *sizeOf(T)
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            // <your code>
            return *(lhs._p) == *(rhs._p);
        }                                           // replace!

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const { // create such that always pointing at a sentinel
            // <your code>
            return *(_p);
        }           // replace!

        // -----------
        // operator ++
        // ----------- [40] 40 bytes [40]

        iterator& operator ++ () { // TODO REVISIT ONCE WE HAVE MORE CODE DONE; HOW MUCH INC POINTER?
            // <your code>
            int sizeOfBlock = abs(*(this->_p)); // dereference the pointer to read the size of the block, currently stored in a sentinel, in terms of bytes
            int sizeOfBlockInTermsOfInts = sizeOfBlock / sizeof(int); // we need to move forward using pointer arithmetic, int pointer, convert to number of ints.
            _p += 1 + sizeOfBlockInTermsOfInts + 1; // jump _p forward 1 int (header sentinel) + size of block (in ints) + 1 int for end sentinel

            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () { // TODO REVISIT ONCE WE HAVE MORE CODE DONE; HOW MUCH DECREMENT POINTER?
            // <your code>
            // need to get from current header sentinel to previous foot; decrement p once
            _p -= 1; // move from current header pointer to previous block's foot.
            int sizeOfBlock = abs(*(this->_p)); // dereference the pointer to read the size of the block, currently stored in end sentinel, in terms of bytes
            int sizeOfBlockInTermsOfInts = sizeOfBlock / sizeof(int); // we need to move back using pointer arithmetic, int pointer, convert to number of ints.
            _p -= (sizeOfBlockInTermsOfInts + 1); // jump _p back size of block (in ints) + 1 int to realign to the header sentinel.
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            // <your code>
            return *(lhs._p) == *(rhs._p);
        }                                                       // replace!

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            // <your code>
            return *(_p);
        }                 // replace!

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            // <your code>
            int sizeOfBlock = abs(*(this->_p)); // dereference the pointer to read the size of the block, currently stored in a sentinel, in terms of bytes
            int sizeOfBlockInTermsOfInts = sizeOfBlock / sizeof(int); // we need to move forward using pointer arithmetic, int pointer, convert to number of ints.
            _p += 1 + sizeOfBlockInTermsOfInts + 1; // jump _p forward 1 int (header sentinel) + size of block (in ints) + 1 int for end sentinel
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () { // TODO REVISIT ONCE WE HAVE MORE CODE DONE; HOW MUCH DECREMENT POINTER?
            // <your code>
            // need to get from current header sentinel to previous foot; decrement p once
            _p -= 1; // move from current header pointer to previous block's foot.
            int sizeOfBlock = abs(*(this->_p)); // dereference the pointer to read the size of the block, currently stored in end sentinel, in terms of bytes
            int sizeOfBlockInTermsOfInts = sizeOfBlock / sizeof(int); // we need to move back using pointer arithmetic, int pointer, convert to number of ints.
            _p -= (sizeOfBlockInTermsOfInts + 1); // jump _p back size of block (in ints) + 1 int to realign to the header sentinel.
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * <your documentation>
     */
    bool valid () const { // maintains that heap state is valid -- TODO
        // <your code>
        // <use iterators>
        // iterator b1 = this->begin();
        // iterator e1 = this->end();
        int* b = (int*)a;
        int* e = (int*)(a+N);
        int sumOfDataStored = 0;
        while (b != e)
        {
            int* header = b;
            assert((abs(*b) + 2 * sizeof(int)) >= (sizeof(T) + 2 * sizeof(int))); // assert that every block is at least the minimum allowed allocation.
            sumOfDataStored += abs(*b) + 2 * sizeof(int); // add 8 bytes for the sentinels, and the block size;
            b += 1 + (abs(*b) / sizeof(int)); // move pointer from header to the footer
            int* footer = b;
            assert(*header == *footer); // make sure that header value == footer value (should always be true, footer assigned the header's value).
            b += 1; // move over footer to next header
        }
        assert(sumOfDataStored == N); // assert that we always have exactly N bytes accommodated for.
        return true;
    }

public:

    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        //(*this)[0] = 0; // replace!
        // <your code>
        (*this)[0] = (N - 8);
        (*this)[N-4] = (N - 8);
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    void printHeapState()
    {
        int* b = (int*)a;
        int* e = (int*)(a+N);
        while (b != e) {
            printf("%d--", *b);
            b += abs(*b) + 1;
            printf("%d ", *b);
            ++b;
        }
    }

    // ---------
    // - split -
    // ---------

    /**
     * We must split the current free block into two new blocks,
     * one busy block satisfying the allocation, and a new, smaller
     * free block which maintains the remainder of the free space.
     */

    pointer splitFreeBlock(int bytesNeedToAllocate, int actualSizeToAllocate, int* header, char* currentIndexChar)
    {
        int busyBytesAllocated = (-1) * bytesNeedToAllocate; // negate the number of bytes we are allocating, to show busy block
        int remainingFreeBytes = *header - actualSizeToAllocate; // remove the actual size we are allocating including sentinels from the block size (excludes sentinels, should create 2 more).
        assert(remainingFreeBytes > 0);
        assert(busyBytesAllocated < 0);
        // adjust for the allocated block
        *header = busyBytesAllocated; // update the value in the header to reflect the allocation
        currentIndexChar += 1 * sizeof(int) + (*header * (-1)); // move the char index past the header to the content, then over the content to the footer.
        int* footer = (int*)currentIndexChar; // create a footer at the new footer location
        *footer = *header; // store the header value in the footer -- maintain invariant.
        // move on to free block
        currentIndexChar += 1 * sizeof(int); // move the char index past the footer to the new free block header location.
        // adjust for the free block
        int* freeBlockHeader = (int*)currentIndexChar; // create a header at the new free block header location
        *freeBlockHeader = remainingFreeBytes; // store the data for the free block here.
        currentIndexChar += 1 * sizeof(int) + *freeBlockHeader; // skip over the header to the content, and over the content to the new free block footer location
        int* freeBlockFooter = (int*)currentIndexChar; // create a footer at the new free block footer location
        *freeBlockFooter = *freeBlockHeader; // store the header value in the footer -- maintain invariant.
        // return the pointer to the allocated block
        pointer contentPointer = (pointer) (header + 1);
        return contentPointer;
    }

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block (otherwise give them the remainder)
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type numberOfObjects) { // number of objects to allocate
        int bytesNeedToAllocate = numberOfObjects * sizeof(T);
        int smallestLegalAllocation = 1 * sizeof(T) + 2 * sizeof(int);
        // iterator b = begin(); // we want to iterate until we find a valid block
        // iterator e = end();
        int* b = (int*)a; // begin and end don't seem to like being used for the my_allocator versions of the tests.
        int* e = (int*)(a+N);
        while (b != e) // so long as we have not reached the end of the heap, we can keep looking
        {
            int* header = b;
            // int* header = &(*b); // Read the header, which is what the iterator is pointing at
            char* currentIndexChar = (char*) header; // USE THIS TO AVOID POINTER ARITHMETIC ERRORS - JUST USE ORIGINAL NUMBERS SINCE SIZEOF CHAR = 1 BYTE.

            int blockSize = *header;
            if (bytesNeedToAllocate <= blockSize) // We only continue with this if there is enough space in the block.
            {
                // ^^^^^^^^^^^^^^ NOTICE WE CAN COMMENT OUT THE PERFECT FIT CASE, SINCE IT IS COVERED BY THE MORE GENERAL CASE BELOW

                // If we reach here, then we have to create a more complex solution - either split the block, or increase the block size.
                int actualSizeToAllocate = bytesNeedToAllocate + 2 * sizeof(int); // need to allocate the sentinels as well.
                if (((blockSize + 2 * ((int)sizeof(int))) - actualSizeToAllocate) < smallestLegalAllocation) // if there is not enough remaining space after allocating for a legal free block, give them all.
                {
                    // then, like perfect fit, simply reuse the sentinels which already exist, no splits
                    *header = *header * (-1);
                    currentIndexChar += (1 * sizeof(int)) + blockSize;
                    int* footer = (int*)currentIndexChar;
                    *footer = *header;
                    pointer contentPointer = (pointer) (header + 1);
                    return contentPointer;
                }
                // ^^^^^ NOTICE THIS IS THE SAME CODE LINE FOR LINE - THEN JUST DO IT ONE TIME.
                return splitFreeBlock(bytesNeedToAllocate, actualSizeToAllocate, header, currentIndexChar); // the parameters might be wrong e.g. pointers and &.
            }
            else // If we enter the else condition, this block cannot possibly contain the desired allocation -- move on.
            {
                b += (abs(*b)/sizeof(int) + 2); // literally just increment iterator.
            }
        }
        // we have iterated through every block in the heap.
        throw std::bad_alloc(); // if we reach here, then the allocation is not supported -- throw bad alloc
        assert(valid());
        return nullptr; // return nullptr because we couldn't allocate the request.
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // -------------------
    // findDeallocateBlock
    // -------------------

    /**
     * Helper method for deallocate, takes in the
     * number busy block we want to free, iterates -- REWRITE IN TERMS OF ITERATORS, ITLL BE WAY SIMPLER.
     * to it, and returns a pointer to it.
     */
    pointer findDeallocateBlock(int numberBusyBlockToFree) // accepts a positive number, processed in RunAllocator
    {
        int* b = (int*)a;
        int* e = (int*)(a+N);
        int busiesFound = 0;
        while (b < e) {
            // printf("{%d}", *b);
            if (*b < 0) {
                ++busiesFound;
                if (busiesFound == numberBusyBlockToFree) {
                    // printf("%d, dealloc index holds this many bytes \n", *b);
                    return (pointer)(b+1);
                }
            }
            b += abs(*b)/sizeof(int) + 2;
        }
        return nullptr;
    }

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * <your documentation>
     */
    void deallocate (pointer p, size_type) { // WHY SIZE TYPE HERE THO
        // <your code>
        if (p == nullptr || *((int*)(p)-1) > 0) {
            //throw invalid_arguement since not a busy block
            throw std::invalid_argument("no block to deallocate");
        }
        int* toBeFreedHead = ((int*)(p)) - 1;
        assert(*toBeFreedHead < 0);
        int* toBeFreedFoot = toBeFreedHead + (abs(*toBeFreedHead) / sizeof(int)) + 1; //(*toBeFreedHead / sizeof(int)) + 1; // P[H] [c] [F] -> [H] [c]P[F]

        int freeBytes = abs(*toBeFreedHead); //should double check my freebytes calculations(?)

        // Check left side
        if (toBeFreedHead != ((int*)a) ) { //these if statements might not work TODO
            //not at beginning check if left free
            int* preFoot = toBeFreedHead - 1;
            if (*preFoot > 0) {
                //prev is free, merge
                toBeFreedHead = preFoot - (1 + *preFoot / sizeof(int)); //move tbfreed head back a block
                freeBytes += *preFoot + 2 * sizeof(int); // absorbing 2 sentinels
            }
        }
        // check right side
        if (toBeFreedFoot != ( (int*)(a + (N-4)) )) {
            //not at end, check if right free
            int* postHead = toBeFreedFoot + 1;
            if (*postHead > 0) {
                //next block free, merge
                toBeFreedFoot = postHead + (1 + *postHead / sizeof(int)); //move tbfreed foot forward a block
                freeBytes += *postHead + 2 * sizeof(int);
            }
        }

        *toBeFreedFoot = freeBytes;
        *toBeFreedHead = freeBytes;

        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(&(*this)[0]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(&(*this)[N]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return const_iterator(&(*this)[N]);
    }
};

#endif // Allocator_h
