// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <string> // c++ strings

#include "Allocator.hpp"

using namespace std;

// ----
// main
// ----

int main () {
    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    */

    // TODO
    // WE NEED TO GET AN APPROPRIATE INSTANCE OF my_allocator

    // read
    string s;
    getline(cin, s); // read in number of tests
    int numberOfTests = stoi(s);
    getline(cin, s); // eat the empty space
    for (int i = 0; i < numberOfTests; i++)
    {
        my_allocator<double, 1000> x;
        while (getline(cin, s)) // checking if eof first means won't get extra line at bottom for last case.
        {
            if (s == "") break;
            // // printf("hi about to print the heap\n");
            // my_allocator<double, 1000>::iterator b = x.begin();
            // // // printf("b made\n");
            // my_allocator<double, 1000>::iterator e = x.end();
            // // printf("e made\n");
            // while (b != e)
            // {
            //     cout << *b << " ";
            //     // printf("finna inc b\n");
            //     ++b;
            //     // printf("inced b, at end?: %d\n", b==e);
            // }
            // printf("exited while loop\n");
            // printf("hi finished printing heap\n");
            int testQuery = stoi(s);
            // printf("testQury: %d \n", testQuery);
            // evaluate
            if (testQuery > 0) // positive value means allocate
            {
                x.allocate(testQuery);
            }
            else if (testQuery < 0) // negative value means deallocate
            {
                // printf("about to deallocate\n");
                // int* p = x.findDeallocateBlock(testQuery * -1);
                // printf("%d", *(p-1));
                // printf("sending in %d to be freed\n", testQuery);
                // double* locatedBusyBlock = x.findDeallocateBlock(testQuery * -1);
                // printf("%p location of deallocating block in RunAlloc\n", (void*)locatedBusyBlock);
                x.deallocate(x.findDeallocateBlock(testQuery * -1), 1);
            }
        }
        // print -- prints the state of the heap after every call.
        my_allocator<double, 1000>::iterator b = x.begin();
        my_allocator<double, 1000>::iterator e = x.end();
        --e;
        while ((int*)(&*b) != (int*)(&*e))
        {
            cout << *b << " ";
            ++b;
        }
        cout << *b << endl;
        // ^^ loop
    }

    return 0;
}
