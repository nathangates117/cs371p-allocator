# CS371p: Object-Oriented Programming Allocator Repo

* Name: (your Full Name)
Nathan Gates, Jesse Huang

* EID: (your EID)
nag2486, jh69268

* GitLab ID: (your GitLab ID)
@nathangates117 @jessehuang

* HackerRank ID: (your HackerRank ID)
@nathangates117 @jesse17huang(submitted)

* Git SHA: (most recent Git SHA, final change to your repo will change this, that's ok)
000ac0ee257497ca2efd820f15858b9204d6875a

* GitLab Pipelines: (link to your GitLab CI Pipeline)
https://gitlab.com/nathangates117/cs371p-allocator/-/pipelines 

* Estimated completion time: (estimated time in hours, int or float)
15

* Actual completion time: (actual time in hours, int or float)
20

* Comments: (any additional comments you have)
